<?php

namespace Drupal\commerce_dellin\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_dellin\Event\CommerceDellinEvents;
use Drupal\commerce_dellin\Event\ShipmentVolumeCalculationEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\physical\LengthUnit;
use Drupal\physical\Volume;
use Drupal\physical\VolumeUnit;
use Drupal\physical\WeightUnit;
use Drupal\profile\Entity\ProfileInterface;
use Niklan\DellinApi\Auth\AppkeyAuth;
use Niklan\DellinApi\Client\ClientInterface;
use Niklan\DellinApi\Client\HttpClient;
use Niklan\DellinApi\Request\Calculation\Pickup;
use Niklan\DellinApi\Request\Catalog\Kladr;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides the Dellin shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "commerce_dellin",
 *   label = @Translation("Dellin"),
 *   services = {
 *     "default" = @Translation("Dellin default delivery"),
 *     "intercity" = @Translation("Dellin to terminal"),
 *     "intercity_door" = @Translation("Dellin to door"),
 *     "air" = @Translation("Dellin air delivery"),
 *     "air_door" = @Translation("Dellin air delivery to the door"),
 *     "express" = @Translation("Dellin express delivery"),
 *     "express_door" = @Translation("Dellin express delivery to the door"),
 *     "small" = @Translation("Dellin small package"),
 *   }
 * )
 */
class Dellin extends ShippingMethodBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'api' => [
          'appkey' => '',
        ],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $description = new TranslatableMarkup('Update your Dellin API information.');
    if (!$this->isConfigured()) {
      $description = new TranslatableMarkup('Fill in your Dellin API information.');
    }
    $form['api_information'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('API information'),
      '#description' => $description,
      '#open' => !$this->isConfigured(),
    ];

    $form['api_information']['appkey'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Application key'),
      '#default_value' => $this->configuration['api_information']['appkey'] ?? '',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => '00000000-0000-0000-0000-000000000000',
      ],
    ];

    $form['arrival_information'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Arrival information'),
      '#description' => new TranslatableMarkup('Fill in your arrival information.'),
      '#open' => TRUE,
    ];

    $form['arrival_information']['arrival_kladr'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('KLADR code of arrival'),
      '#description' => new TranslatableMarkup('KLADR code of the point, from which will be calculated delivery price.'),
      '#default_value' => $this->configuration['arrival_information']['arrival_kladr'] ?? '',
    ];

    return $form;
  }

  /**
   * Determines is plugin is configured.
   *
   * @return bool
   *   TRUE if all necessary information is set, FALSE otherwise.
   */
  protected function isConfigured() {
    return !empty($this->configuration['api_information']['appkey']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['api_information']['appkey'] = $values['api_information']['appkey'];
    $this->configuration['arrival_information']['arrival_kladr'] = $values['arrival_information']['arrival_kladr'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $shipping_profile = $shipment->getShippingProfile();
    if (!$shipping_profile->hasField('address')) {
      return [];
    }

    if (!$shipment->hasItems()) {
      return [];
    }

    $auth = new AppkeyAuth($this->configuration['api_information']['appkey']);
    $client = new HttpClient($auth);

    $weight = $shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber();
    $volume = $this->getShipmentVolume($shipment);
    if (!$volume) {
      return [];
    }

    $stated_value = $shipment->getTotalDeclaredValue()->getNumber();

    $rates = [];
    $rates_to_terminal = $this->calculateRatesByDellin($client, $shipment, $weight, $volume, $stated_value);
    $rates = array_merge($rates, $rates_to_terminal);

    return $rates;
  }

  /**
   * Gets shipment volume.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return string|null
   *   The volume.
   */
  protected function getShipmentVolume(ShipmentInterface $shipment) {
    $volume = NULL;

    $event = new ShipmentVolumeCalculationEvent($shipment);
    $this->eventDispatcher->dispatch($event, CommerceDellinEvents::SHIPMENT_VOLUME_CALCULATION);
    $volume = $event->getVolume();

    if (!$volume instanceof Volume) {
      $package_type = $shipment->getPackageType();
      $length = $package_type->getLength()->convert(LengthUnit::METER)->getNumber();
      $width = $package_type->getWidth()->convert(LengthUnit::METER)->getNumber();
      $height = $package_type->getHeight()->convert(LengthUnit::METER)->getNumber();
      // This is to avoid exponential value. E.g. 0.5*0.015*0.002=1.5E-5 which
      // is actually 0.000015.
      $volume_value = number_format($length * $width * $height, 6);
      $volume = new Volume($volume_value, VolumeUnit::CUBIC_METER);
    }

    // The volume required for calc price.
    if (!$volume instanceof Volume) {
      return NULL;
    }

    return $volume->convert(VolumeUnit::CUBIC_METER)->getNumber();
  }

  /**
   * Calculates rates for terminal delivery.
   *
   * @param \Niklan\DellinApi\Client\ClientInterface $client
   *   An API client.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param string $weight
   *   The weight.
   * @param string $volume
   *   The volume.
   * @param string $stated_value
   *   The stated value.
   *
   * @return array
   *   An array with rates.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function calculateRatesByDellin(ClientInterface $client, ShipmentInterface $shipment, $weight, $volume, $stated_value) {
    $derrival_kladr = $this->findDestinationKladr($client, $shipment->getShippingProfile());
    if (!$derrival_kladr) {
      return [];
    }

    $pickup = new Pickup($derrival_kladr, $this->configuration['arrival_information']['arrival_kladr'], $volume, $weight);
    $pickup->setStatedValue($stated_value);
    $pickup->setArrivalDoor(TRUE);
    $response = $client->execute($pickup);

    if (!$response->isSuccess()) {
      return [];
    }

    $rates = [];
    $result = $response->getResult();

    $delivery_date = new DrupalDateTime('now');
    $delivery_date->modify('+ ' . $result['time']['value'] . ' days');

    // service_name => options.
    $rates_to_calculate = [
      'intercity' => [
        'result' => 'intercity',
        'options' => [],
      ],
      'intercity_door' => [
        'result' => 'intercity',
        'options' => ['include_arrival' => TRUE],
      ],
      'air' => [
        'result' => 'air',
        'options' => [],
      ],
      'air_door' => [
        'result' => 'air',
        'options' => ['include_arrival' => TRUE],
      ],
      'express' => [
        'result' => 'express',
        'options' => [],
      ],
      'express_door' => [
        'result' => 'express',
        'options' => ['include_arrival' => TRUE],
      ],
      'small' => [
        'result' => 'small',
        'options' => [],
      ],
    ];

    foreach ($rates_to_calculate as $service => $config) {
      // Skip inactive services.
      if (!\array_key_exists($service, $this->getServices())) {
        continue;
      }
      // Skip if expected result is missing in API response result.
      if (!isset($result[$config['result']])) {
        continue;
      }

      $amount = new Price(0, 'RUB');
      $amount = $this->getDeliveryTotal($amount, $result, $config['result'], $config['options']);
      $rates[] = new ShippingRate([
        'shipping_method_id' => $this->parentEntity->id(),
        'service' => $this->services[$service],
        'amount' => $amount,
        'delivery_date'
      ]);
    }

    if (\array_key_exists('default', $this->getServices())) {
      // Default automatic delivery method. It can be missing for contracts.
      if (isset($result['price']) && !empty($result['price'])) {
        // Dellin price is 'auto' selected method. It didn't tell us what method
        // was used, so we cant filter out easily results. This leads to a
        // problem when we can have duplicates entries for same amount, it can
        // confuse.
        //
        // We remove default delivery method if we found the exact same amount
        // on the other active rates, which will be more clear to customer,
        // because they are specific about method of delivery.
        //
        // Also, take into account that in some cases there will be only 'price'
        // in the response. So we can't just find the same amount and assign it.
        $is_same_amount_found = FALSE;
        $price = new Price($result['price'], 'RUB');
        foreach ($rates as $rate) {
          if ($rate->getAmount()->equals($price)) {
            $is_same_amount_found = TRUE;
            break;
          }
        }
        if (!$is_same_amount_found) {
          $rates[] = new ShippingRate([
            'shipping_method_id' => $this->parentEntity->id(),
            'service' => $this->services['default'],
            'amount' => new Price($result['price'], 'RUB'),
            'delivery_date',
          ]);
        }
      }
    }

    return $rates;
  }

  /**
   * Finds KLADR code for provided shipping profile.
   *
   * @param \Niklan\DellinApi\Client\ClientInterface $client
   *   An API client.
   * @param \Drupal\profile\Entity\ProfileInterface $shipping_profile
   *   The shipping profile.
   *
   * @return string|null
   *   The KLADR code if found.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function findDestinationKladr(ClientInterface $client, ProfileInterface $shipping_profile) {
    if (!$shipping_profile->hasField('address')) {
      return NULL;
    }

    if ($shipping_profile->get('address')->isEmpty()) {
      return NULL;
    }

    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $shipping_profile->get('address')->first();
    // The KLADR is Russian government code. Also, dellin doesn't support
    // deliveries in other countries.
    if ($address->getCountryCode() != 'RU') {
      return NULL;
    }

    $city_request = new Kladr();
    $city_request->searchByQuery($address->getLocality());
    $city_response = $client->execute($city_request);
    $city_result = $city_response->getResult();
    if (!isset($city_result['cities'][0])) {
      return NULL;
    }

    return $city_result['cities'][0]['code'];
  }

  /**
   * Gets delivery total price.
   *
   * @param \Drupal\commerce_price\Price $base_price
   *   The base price to add components to.
   * @param array $result
   *   The result from Pickup dellin API.
   * @param string $delivery_type
   *   The delivery type: air, express, intercity.
   * @param array $options
   *   The options override.
   *
   * @return \Drupal\commerce_price\Price
   *   The total amount.
   */
  protected function getDeliveryTotal(Price $base_price, array $result, $delivery_type, $options = []) {
    $default_options = [
      'include_arrival' => FALSE,
      'include_insurance' => TRUE,
      'include_notify' => TRUE,
    ];
    $options = $options + $default_options;

    $delivery_price = new Price($result[$delivery_type]['price'], 'RUB');
    $base_price = $base_price->add($delivery_price);

    if ($options['include_insurance']) {
      $insurance_price = new Price($result[$delivery_type]['insurance'], 'RUB');
      $base_price = $base_price->add($insurance_price);
    }

    if ($options['include_notify']) {
      $notify_price = new Price($result[$delivery_type]['notify']['price'], 'RUB');
      $base_price = $base_price->add($notify_price);
    }

    if ($options['include_arrival']) {
      $arrival_price = new Price($result['arrival']['price'], 'RUB');
      $base_price = $base_price->add($arrival_price);
    }

    return $base_price;
  }

}

