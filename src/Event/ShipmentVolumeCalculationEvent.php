<?php

namespace Drupal\commerce_dellin\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\physical\Volume;
use Drupal\physical\VolumeUnit;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides event for calculation volume.
 */
class ShipmentVolumeCalculationEvent extends Event {

  /**
   * The shipment entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * The volume.
   *
   * @var \Drupal\physical\Volume
   */
  protected $volume = NULL;

  /**
   * Constructs a new ShipmentVolumeCalculationEvent object.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   */
  public function __construct(ShipmentInterface $shipment) {
    $this->shipment = $shipment;
  }

  /**
   * Gets shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment.
   */
  public function getShipment() {
    return $this->shipment;
  }

  /**
   * Sets the shipment volume.
   *
   * @param \Drupal\physical\Volume $volume
   *   The volume.
   */
  public function setVolume(Volume $volume) {
    $this->volume = $volume->convert(VolumeUnit::CUBIC_METER);
  }

  /**
   * Gets the volume.
   *
   * @return \Drupal\physical\Volume|null
   *   The volume or NULL if not set.
   */
  public function getVolume() {
    return $this->volume;
  }

}
