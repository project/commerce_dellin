<?php

namespace Drupal\commerce_dellin\Event;

/**
 * Provides definitions for module events.
 */
final class CommerceDellinEvents {

  /**
   * Provides event for set shipment volume.
   *
   * @Event
   *
   * @see \Drupal\commerce_dellin\Event\ShipmentVolumeCalculationEvent
   * @see \Drupal\commerce_dellin\Plugin\Commerce\ShippingMethod\Dellin
   */
  const SHIPMENT_VOLUME_CALCULATION = 'commerce_dellin.shipment_volume_calculation';

}
